import React, { Component } from 'react';
import PropTypes from 'prop-types';
import createDataSource from '../hackernews/datasource';
import Search from '../Search';
import Table from '../Table';
import Button from '../Button';
import { withLoading } from '../Loading';
import './App.css';

class App extends Component {
  static propTypes = {
    dataSource: PropTypes.shape({
      search: PropTypes.func.isRequired,
      page: PropTypes.func.isRequired
    })
  };

  static defaultProps = {
    dataSource: createDataSource()
  };

  constructor(props) {
    super(props);
    this.state = {
      searchTerm: 'redux',
      results: null,
      isLoading: false
    };
    this.dataSource = { ...this.props.dataSource };
  }

  async componentDidMount() {
    this.setState({ isLoading: true });
    const results = await this.dataSource.search({
      query: this.state.searchTerm
    });
    this.setState({ results: results, isLoading: false });
  }

  onSearchChange = event => {
    this.setState({ searchTerm: event.target.value });
  };

  onSearchSubmit = async event => {
    event.preventDefault();
    this.setState({ isLoading: true });
    const results = await this.dataSource.search({
      query: this.state.searchTerm
    });
    this.setState({ results: results, isLoading: false });
  };

  onDismiss = objectID => {
    this.setState(prevState => {
      return {
        results: prevState.results.filter(item => item.objectID !== objectID)
      };
    });
  };

  onRequestMore = async () => {
    const { searchTerm } = this.state;
    this.setState({ isLoading: true });
    const results = await this.dataSource.page({
      query: searchTerm
    });
    this.setState({ results: results, isLoading: false });
  };

  render() {
    const { results, searchTerm } = this.state;
    const ButtonWithLoading = withLoading(Button);

    return (
      <div className="page">
        <div className="interactions">
          <Search
            value={searchTerm}
            onChange={this.onSearchChange}
            onSubmit={this.onSearchSubmit}
          >
            Search
          </Search>
        </div>
        {results && <Table list={results} onDismiss={this.onDismiss} />}
        <div className="interactions">
          <ButtonWithLoading
            isLoading={this.state.isLoading}
            onClick={this.onRequestMore}
          >
            More
          </ButtonWithLoading>
        </div>
      </div>
    );
  }
}

export default App;
