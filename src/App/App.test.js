import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import App from './';

describe('App', () => {
  const element = <App dataSource={createDataSourceMock()} />;

  it('renders', () => {
    const div = document.createElement('div');
    ReactDOM.render(element, div);
  });

  it('matches the snapshot', () => {
    const component = renderer.create(element, { createNodeMock });
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});

function createNodeMock() {
  return {
    focus() {}
  };
}

function createDataSourceMock() {
  return {
    search: () => [],
    page: () => []
  };
}
