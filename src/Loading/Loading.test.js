import React from 'react';
import renderer from 'react-test-renderer';
import Loading, { withLoading } from './';

describe('Loading', () => {
  it('matches the snapshot', () => {
    const component = renderer.create(<Loading />);
    expect(component.toJSON()).toMatchSnapshot();
  });
});

describe('withLoading', () => {
  const ComponentWithLoading = withLoading(() => <div>A component</div>);

  describe('when loading', () => {
    it('matches the snapshot', () => {
      const component = renderer.create(
        <ComponentWithLoading isLoading={true} />
      );
      expect(component.toJSON()).toMatchSnapshot();
    });
  });

  describe('when not loading', () => {
    it('matches the snapshot', () => {
      const component = renderer.create(
        <ComponentWithLoading isLoading={false} />
      );
      expect(component.toJSON()).toMatchSnapshot();
    });
  });
});
