import React from 'react';
import 'font-awesome/css/font-awesome.css';

const Loading = () => <i className="fa fa-spinner fa-spin" />;

export default Loading;

export function withLoading(Component) {
  return ({ isLoading, ...props }) => {
    if (isLoading) {
      return <Loading />;
    } else {
      return <Component {...props} />;
    }
  }
}
