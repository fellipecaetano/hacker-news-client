import { create } from './api';
export default create({ fetch: window.fetch, limit: 5 });
