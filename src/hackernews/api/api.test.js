import { create, URL_TEMPLATE } from './api';
import { parse as parseUrl } from 'url';

describe('Hacker News API', async () => {
  describe('search', () => {
    it('calls "fetch" with the expected URL', async () => {
      expect.assertions(2);
      const api = create({ fetch: mockFetch, limit: 15 });
      const options = { query: 'redux', page: '10' };
      const { query, protocol, pathname, host } = await api.search(options);
      expect({ protocol, pathname, host }).toEqual(URL_TEMPLATE);
      expect(query).toEqual({ hitsPerPage: '15', ...options });
    });
  });

  describe('page', () => {
    it('calls "fetch" with the expected URL when offset is positive', async () => {
      const api = create({ fetch: mockFetch, limit: 15 });
      const options = { query: 'react', offset: 45 };
      const { query, protocol, pathname, host } = await api.page(options);
      expect({ protocol, pathname, host }).toEqual(URL_TEMPLATE);
      expect(query).toEqual({
        query: options.query,
        page: '3',
        hitsPerPage: '15'
      });
    });

    it('calls "fetch" with the expected URL when offset is zero', async () => {
      const api = create({ fetch: mockFetch, limit: 15 });
      const options = { query: 'react', offset: 0 };
      const { query, protocol, pathname, host } = await api.page(options);
      expect({ protocol, pathname, host }).toEqual(URL_TEMPLATE);
      expect(query).toEqual({
        query: options.query,
        page: '0',
        hitsPerPage: '15'
      });
    });
  });
});

function mockFetch(url) {
  return new Promise(resolve => {
    resolve({
      json: () => parseUrl(url, true)
    });
  });
}
