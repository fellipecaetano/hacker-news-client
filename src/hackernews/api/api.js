import { format as formatUrl } from 'url';

export const URL_TEMPLATE = {
  protocol: 'https:',
  host: 'hn.algolia.com',
  pathname: '/api/v1/search'
};

export function create({ fetch, limit }) {
  return {
    search(query) {
      const url = formatUrl({
        ...URL_TEMPLATE,
        query: { hitsPerPage: limit, ...query }
      });
      return fetch(url).then(response => response.json());
    },

    page({ query, offset }) {
      const page = offset / limit + (offset % limit > 0 ? 1 : 0);
      return this.search({ query, page });
    }
  };
}
