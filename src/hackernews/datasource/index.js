import { create as createDataSource } from './datasource';
import api from '../api';

export default function create() {
  const dataSource = createDataSource(api);
  return {
    search: options => dataSource.search(options),
    page: options => dataSource.page(options)
  }
}
