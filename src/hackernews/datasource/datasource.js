export function create(api) {
  const select = results => results.hits;
  return {
    cache: {},

    async search(options) {
      if (this.cache[options.query]) {
        return this.cache[options.query];
      }
      const results = await api.search(options).then(results => select(results));
      this.cache[options.query] = results;
      return results;
    },

    async page({ query }) {
      const cached = this.cache[query] || [];
      const results = await api.page({
        offset: cached.length,
        query
      }).then(results => select(results));
      this.cache[query] = [...cached, ...results];
      return [...cached, ...results];
    }
  };
}
