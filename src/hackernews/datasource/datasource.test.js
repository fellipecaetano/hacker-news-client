import { create } from './datasource';

describe('Hacker News data source', () => {
  const hits = createMockHits({ objectIDs: ['1', '2', '3'] });

  describe('search', () => {
    it('returns results', async () => {
      expect.assertions(1);
      const dataSource = create({
        search: async () => ({ hits })
      });
      const results = await dataSource.search({ query: 'redux' });
      expect(results).toEqual(hits);
    });

    it('caches results grouped by query', async () => {
      expect.assertions(1);
      const dataSource = create({
        search: async () => ({ hits })
      });
      const results = await dataSource.search({ query: 'react' });
      expect(dataSource.cache['react']).toEqual(results);
    });

    it('hits the cache instead of the API when possible', async () => {
      expect.assertions(1);
      const cachedHits = createMockHits({ objectIDs: ['4', '5'] });
      const dataSource = Object.assign(
        {},
        create({
          search: async () => ({ hits })
        }),
        {
          cache: {
            haskell: cachedHits
          }
        }
      );
      const results = await dataSource.search({ query: 'haskell' });
      expect(results).toEqual(cachedHits);
    });
  });

  describe('page', () => {
    it('returns results', async () => {
      expect.assertions(1);
      const dataSource = create({
        page: async () => ({ hits })
      });
      const results = await dataSource.page({ query: 'react' });
      expect(results).toEqual(hits);
    });

    it('calculates offsets based on the cache', async () => {
      expect.assertions(1);
      const dataSource = Object.assign(
        {},
        create({
          page: async ({ offset }) => {
            expect(offset).toEqual(3);
            return { hits };
          }
        }),
        {
          cache: {
            rails: hits
          }
        }
      );
      await dataSource.page({ query: 'rails' });
    });

    it('appends results to the cache', async () => {
      expect.assertions(2);
      const cachedHits = createMockHits({ objectIDs: ['4', '5'] });
      const dataSource = Object.assign(
        {},
        create({
          page: async ({ offset }) => ({ hits })
        }),
        {
          cache: {
            rails: cachedHits,
            redux: hits
          }
        }
      );
      await dataSource.page({ query: 'rails' });
      expect(dataSource.cache['rails']).toEqual([...cachedHits, ...hits]);
      expect(dataSource.cache['redux']).toEqual(hits);
    });
  });

  it('returns results appended from the cache', async () => {
    expect.assertions(1);
    const cachedHits = createMockHits({ objectIDs: ['6', '7'] });
    const dataSource = Object.assign(
      {},
      create({
        page: async ({ offset }) => ({ hits })
      }),
      {
        cache: {
          rails: cachedHits
        }
      }
    );
    const results = await dataSource.page({ query: 'rails' });
    expect(results).toEqual([...cachedHits, ...hits]);
  });
});

function createMockHits({ objectIDs }) {
  return objectIDs.map(objectID => ({
    objectID,
    url: `http://news.ycombinator.com/${objectID}`,
    title: `Title ${objectID}`,
    author: `Author ${objectID}`,
    num_comments: parseInt(objectID, 10),
    points: parseInt(objectID, 10) * 5
  }));
}
