import { sortBy } from 'lodash';

function createSorter(...keys) {
  const sorter = keys.reduce((sorter, key) => {
    return { ...sorter, [key]: key };
  }, {});

  return {
    ...sorter,

    keys() {
      return [...keys];
    }
  };
}

export default Object.assign(
  {},
  createSorter('TITLE', 'AUTHOR', 'COMMENTS', 'POINTS', 'NONE'),
  {
    sort(list, { key }) {
      switch (key) {
        case this.TITLE:
          return sortBy(list, 'title');
        case this.AUTHOR:
          return sortBy(list, 'author');
        case this.COMMENTS:
          return sortBy(list, 'num_comments').reverse();
        case this.POINTS:
          return sortBy(list, 'points').reverse();
        default:
          return list;
      }
    }
  }
);
