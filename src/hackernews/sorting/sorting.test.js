import sorting from './';

describe('Hacker News story sorting', () => {
  describe('keys', () => {
    it('returns all sort keys', () => {
      expect(sorting.keys()).toEqual([
        sorting.TITLE,
        sorting.AUTHOR,
        sorting.COMMENTS,
        sorting.POINTS,
        sorting.NONE
      ]);
    });
  });

  describe('sort', () => {
    const stories = [
      {
        title: 'zYrn',
        author: 'uJl',
        num_comments: 9,
        points: 2,
        objectID: 'y'
      },
      {
        title: 'Kmaae',
        author: 'a9k(',
        num_comments: 1,
        points: 4,
        objectID: 'z'
      },
      {
        title: 'fg',
        author: '19ks',
        num_comments: 14,
        points: 60,
        objectID: 'd'
      },
      {
        title: '_jsi',
        author: 'ak(3',
        num_comments: 4,
        points: 11,
        objectID: 'Iis'
      }
    ];

    it('sorts by title', () => {
      const titles = sorting
        .sort(stories, { key: sorting.TITLE })
        .map(s => s.title);
      expect(titles).toEqual(['Kmaae', '_jsi', 'fg', 'zYrn']);
    });

    it('sorts by author', () => {
      const authors = sorting
        .sort(stories, { key: sorting.AUTHOR })
        .map(s => s.author);
      expect(authors).toEqual(['19ks', 'a9k(', 'ak(3', 'uJl']);
    });

    it('sorts by comment count, descending', () => {
      const comments = sorting
        .sort(stories, { key: sorting.COMMENTS })
        .map(s => s.num_comments);
      expect(comments).toEqual([14, 9, 4, 1]);
    });

    it('sorts by points, descending', () => {
      const points = sorting
        .sort(stories, { key: sorting.POINTS })
        .map(s => s.points);
      expect(points).toEqual([60, 11, 4, 2]);
    });
  });
});
