import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import Table from './';

describe('Table', () => {
  const props = {
    list: [
      { title: 'z', author: 'uJl', num_comments: 9, points: 2, objectID: 'y' },
      { title: 'K', author: '9k(', num_comments: 1, points: 4, objectID: 'z' },
      { title: 'fg', author: '19ks', num_comments: 14, points: 60, objectID: 'd' },
      { title: '_jsi', author: 'ak(3', num_comments: 4, points: 11, objectID: 'Iis' }
    ]
  };

  it('renders', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Table {...props} />, div);
  });

  it('shows four items in the list', () => {
    const rendered = shallow(<Table {...props} />);
    expect(rendered.find('.table-row').length).toBe(4);
  });

  describe('when not sorted', () => {
    it('matches the snapshot', () => {
      const component = renderer.create(
        <Table {...props} initialSortKey="NONE" />
      );
      expect(component.toJSON()).toMatchSnapshot();
    });
  });

  describe('when sorted by title', () => {
    it('matches the snapshot', () => {
      const component = renderer.create(
        <Table {...props} initialSortKey="TITLE" />
      );
      expect(component.toJSON()).toMatchSnapshot();
    });
  });

  describe('when sorted by author', () => {
    it('matches the snapshot', () => {
      const component = renderer.create(
        <Table {...props} initialSortKey="AUTHOR" />
      );
      expect(component.toJSON()).toMatchSnapshot();
    });
  });

  describe('when sorted by comments', () => {
    it('matches the snapshot', () => {
      const component = renderer.create(
        <Table {...props} initialSortKey="COMMENTS" />
      );
      expect(component.toJSON()).toMatchSnapshot();
    });
  });

  describe('when sorted by points', () => {
    it('matches the snapshot', () => {
      const component = renderer.create(
        <Table {...props} initialSortKey="POINTS" />
      );
      expect(component.toJSON()).toMatchSnapshot();
    });
  });
});
