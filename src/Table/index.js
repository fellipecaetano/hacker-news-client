import React, { Component } from 'react';
import PropTypes from 'prop-types';
import sorting from '../hackernews/sorting';
import Button from '../Button';
import SortButton from '../SortButton';

class Table extends Component {
  static propTypes = {
    list: PropTypes.arrayOf(
      PropTypes.shape({
        objectID: PropTypes.string.isRequired,
        url: PropTypes.string,
        title: PropTypes.string,
        author: PropTypes.string,
        num_comments: PropTypes.number,
        points: PropTypes.number
      })
    ),
    onDismiss: PropTypes.func,
    initialSortKey: PropTypes.oneOf(sorting.keys())
  };

  static defaultProps = {
    initialSortKey: sorting.NONE
  };

  constructor(props) {
    super(props);
    this.state = {
      sortKey: props.initialSortKey
    };
  }

  onSort = key => {
    this.setState({ sortKey: key });
  };

  sortedList = () => {
    return sorting.sort(this.props.list, { key: this.state.sortKey });
  };

  render() {
    const largeColumn = {
      width: '40%'
    };

    const midColumn = {
      width: '30%'
    };

    const smallColumn = {
      width: '10%'
    };

    const { onDismiss } = this.props;
    const { sortKey } = this.state;

    return (
      <div className="table">
        <div className="table-header">
          <span style={largeColumn}>
            <SortButton
              sortKey={sorting.TITLE}
              activeSortKey={sortKey}
              onSort={this.onSort}
            >
              Title
            </SortButton>
          </span>
          <span style={midColumn}>
            <SortButton
              sortKey={sorting.AUTHOR}
              activeSortKey={sortKey}
              onSort={this.onSort}
            >
              Author
            </SortButton>
          </span>
          <span style={smallColumn}>
            <SortButton
              sortKey={sorting.COMMENTS}
              activeSortKey={sortKey}
              onSort={this.onSort}
            >
              Comments
            </SortButton>
          </span>
          <span style={smallColumn}>
            <SortButton
              sortKey={sorting.POINTS}
              activeSortKey={sortKey}
              onSort={this.onSort}
            >
              Points
            </SortButton>
          </span>
          <span style={smallColumn}>
            Archive
          </span>
        </div>
        {this.sortedList().map(item => {
          return (
            <div key={item.objectID} className="table-row">
              <span style={largeColumn}>
                <a href={item.url}>{item.title}</a>
              </span>
              <span style={midColumn}>{item.author}</span>
              <span style={smallColumn}>{item.num_comments}</span>
              <span style={smallColumn}>{item.points}</span>
              <span style={smallColumn}>
                <Button
                  onClick={() => onDismiss(item.objectID)}
                  className="button-inline"
                >
                  Dismiss
                </Button>
              </span>
            </div>
          );
        })}
      </div>
    );
  }
}

export default Table;
