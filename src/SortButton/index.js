import React from 'react';
import PropTypes from 'prop-types';
import Button from '../Button';

const SortButton = ({ sortKey, activeSortKey, onSort, children }) => {
  const className = () => {
    if (sortKey === activeSortKey) {
      return ['button-inline', 'button-active'].join(' ');
    } else {
      return 'button-inline';
    }
  };
  const onClick = () => onSort(sortKey);
  return <Button className={className()} onClick={onClick}>{children}</Button>;
};

SortButton.propTypes = {
  sortKey: PropTypes.string.isRequired,
  activeSortKey: PropTypes.string.isRequired,
  onSort: PropTypes.func
};

export default SortButton;
