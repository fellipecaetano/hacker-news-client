import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import SortButton from './';

describe('SortButton', () => {
  describe('when not active', () => {
    const element = (
      <SortButton sortKey="a873uh" activeSortKey="ja9s9">Field</SortButton>
    );

    it('matches the snapshot', () => {
      const rendered = renderer.create(element);
      expect(rendered.toJSON()).toMatchSnapshot();
    });

    it('does not indicate being active', () => {
      const rendered = shallow(element);
      expect(rendered.find('.button-active').length).toEqual(0);
    });
  });

  describe('when active', () => {
    const element = (
      <SortButton sortKey="a873uh" activeSortKey="a873uh">Field</SortButton>
    );

    it('matches the snapshot', () => {
      const rendered = renderer.create(element);
      expect(rendered.toJSON()).toMatchSnapshot();
    });

    it('indicates being active', () => {
      const rendered = shallow(element);
      expect(rendered.find('.button-active').length).toEqual(1);
    });
  });

  describe('when sorting', () => {
    it('calls the handler with the supplied key', () => {
      expect.assertions(1);
      const onSort = key => expect(key).toEqual('a873uh');
      const rendered = shallow(
        <SortButton sortKey="a873uh" activeSortKey="ja9s9" onSort={onSort}>
          Field
        </SortButton>
      );
      rendered.find('Button').simulate('click');
    });
  });
});
