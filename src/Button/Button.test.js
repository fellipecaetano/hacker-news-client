import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import Button from './';

describe('Button', () => {
  const element = <Button>Give Me More</Button>;

  it('renders', () => {
    const div = document.createElement('div');
    ReactDOM.render(element, div);
  });

  it('matches the snapshot', () => {
    const component = renderer.create(element);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
