import React from 'react';
import PropTypes from 'prop-types';

const Button = ({ className, type = 'button', onClick, children }) => {
  return (
    <button type={type} className={className} onClick={onClick}>
      {children}
    </button>
  );
};

Button.propTypes = {
  className: PropTypes.string,
  type: PropTypes.string,
  onClick: PropTypes.func,
  children: PropTypes.node
};

export default Button;
