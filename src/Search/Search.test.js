import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import Search from './';

describe('Search', () => {
  const element = <Search value="query" onChange={() => false}>Search</Search>;

  it('renders', () => {
    const div = document.createElement('div');
    ReactDOM.render(element, div);
  });

  it('matches the snapshot', () => {
    const component = renderer.create(element, { createNodeMock });
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});

function createNodeMock() {
  return {
    focus() {}
  };
}
