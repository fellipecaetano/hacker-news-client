import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '../Button';

class Search extends Component {
  static propTypes = {
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func,
    onSubmit: PropTypes.func,
    children: PropTypes.node
  };

  componentDidMount() {
    this.input.focus();
  }

  render() {
    const { value, onChange, onSubmit, children } = this.props;

    return (
      <form onSubmit={onSubmit}>
        <input
          type="text"
          onChange={onChange}
          value={value}
          ref={input => (this.input = input)}
        />
        <Button type="submit">{children}</Button>
      </form>
    );
  }
}

export default Search;
